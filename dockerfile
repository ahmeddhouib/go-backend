FROM golang:1.11.1-alpine3.8
RUN mkdir /go-bakend
ADD . /go-bakend
WORKDIR /go-bakend
#RUN CD docker-backend
# ENV  GO111MODULE=on
# COPY go.mod .
# COPY go.sum .
#RUN go mod init github.com/TutorialEdge/realtime-chat-go-react
RUN go mod download
# RUN go get github.com/gorilla/websocket
RUN go build -o main .
CMD ["/go-bakend/main"]